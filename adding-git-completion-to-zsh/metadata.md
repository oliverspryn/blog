# Metadata

- **Tags:** Bash, Git, Shell, Terminal, Zsh
- **SEO Title:** Adding Git Completion to Zsh
- **SEO Description:** Learn how to manually set up Zsh to provide fast, easy Git completion for all of your software projects.
- **Kicker:** The handguide for a manual setup
- **Subtitle:** Almost as easy as a brew install
