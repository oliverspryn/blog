# Adding Git Completion to Zsh

I recently switched my shell from Bash to Zsh, and after installing my new favorite extensions ([Powerlevel10k](https://oliverspryn.link/powerlevel10) and [Meslo Nerd Font](https://oliverspryn.link/nerd-fonts)), I realized I was missing a key component from Bash: Git completion.

Since we don’t all have the luxury of running `brew install bash-completion` and following the associated directions, I’ll discuss how I manually installed the shell scripts necessary to support Git completion in Zsh.

![Layered terminal windows showing a directory listing and htop](./assets/terminal.jpg)

## Finding the Dependencies

The Git community maintains all the shell completion scripts in [their repository on GitHub](https://oliverspryn.link/git-completion-repository). Navigating to this folder reveals scripts for a variety of shells, including Bash and Zsh.

![Git's shell completion scripts on GitHub](./assets/git-completion-repository.jpg)

Since both Bash and Zsh are based on the Bourne shell, they tend to [share many similarities](https://oliverspryn.link/z-shell-faq). Git makes this pretty evident after one opens the `git-completion.zsh` and the comment at the top states:

> You need git’s bash completion script installed somewhere, by default it would be the location bash-completion uses.

Thus, we need two scripts to have Git completion in its entirety.

## Downloading the Scripts

Now that it is clear which scripts are necessary for full Git completion functionality, let’s get them:

```sh
# Create the folder structure
mkdir -p ~/.zsh
cd ~/.zsh
# Download the scripts
curl -o git-completion.bash https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
curl -o _git https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh
```

Notice how, by convention, I placed the shell scripts in a folder called `.zsh` under the user’s home folder.

## Configuring the Shell

Once the scripts are in position, the `.zshrc` file may be configured to provide access to their functionality. Open up the `~/.zshrc` file and add the following lines of code:

```sh
# Load Git completion
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
fpath=(~/.zsh $fpath)
autoload -Uz compinit && compinit
```

On your first run, make sure you clear out the shell’s autocompletion cache with:

```sh
rm ~/.zcompdump
```

Then save the script, reload your shell, and voila!

![An animation showing the result of the configuration](./assets/final-result.gif)

For the curious reader, here’s a quick summary of what the above configuration script accomplishes:

* `zstyle`: Instructs the shell on how to provide inline, contextual hints
* `fpath`: The `git-completion.zsh` is a function file, not designed to be `sourced` like the bash script. This command appends the `~/.zsh` directory onto the shell’s function lookup list.
* `autoload`: Scan each path within the `fpath` variable for files starting with an underscore (`_git`, in our case) and loads the corresponding script as a function file
* `compinit`: Initializes the shell’s auto-completion functionality

## References

These links proved themselves immensely helpful as I learned how to understand and configure this on my own:

* [Git Completion Script in Zsh](https://oliverspryn.link/git-completion-script-in-zsh)
* [Zsh Completion System](https://oliverspryn.link/git-completion-system)
* [Zsh Styling](https://oliverspryn.link/zsh-styling)
* [Zsh Functions](https://oliverspryn.link/zsh-functions)
* [Using compinit](https://oliverspryn.link/using-compinit)
