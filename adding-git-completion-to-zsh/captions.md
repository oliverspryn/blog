# Captions

- By [Sai Kiran Anagani](https://unsplash.com/@_imkiran?utm_source=medium&utm_medium=referral) on [Unsplash](https://unsplash.com?utm_source=medium&utm_medium=referral)
- Git's shell completion scripts [on GitHub](https://oliverspryn.link/git-completion-repository)
- Commands to download all the Git completion scripts
- Configures the shell to include and use the Git completion scripts
- Zsh will reload the autocomplete cache on its next run
- The result of the configuration (along with some [Powerlevel10k](https://oliverspryn.link/powerlevel10) polish)
