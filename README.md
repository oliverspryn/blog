# Oliver Spryn's Blog

This is collection of blog posts available on my [personal blog](https://oliverspryn.blog/), [Medium](https://oliverspryn.medium.com/), the [DEV Community](https://dev.to/oliverspryn), [Hashnode](https://oliverspryn.hashnode.dev/), and [LinkedIn](https://www.linkedin.com/in/oliverspryn/detail/recent-activity/posts/).

* [**Conditional Gradle Configuration by Build Variant**](./conditional-build-variant/README.md) originally published on October 19, 2018
  * Blog: [https://oliverspryn.blog/conditional-gradle-configuration-by-build-variant](https://oliverspryn.blog/conditional-gradle-configuration-by-build-variant)
  * Medium: [https://oliverspryn.medium.com/conditional-gradle-configuration-by-build-variant-331695240598](https://oliverspryn.medium.com/conditional-gradle-configuration-by-build-variant-331695240598)
  * DEV Community: [https://dev.to/oliverspryn/conditional-gradle-configuration-by-build-variant-50lh](https://dev.to/oliverspryn/conditional-gradle-configuration-by-build-variant-50lh)
  * Hashnode: [https://oliverspryn.hashnode.dev/conditional-gradle-configuration-by-build-variant](https://oliverspryn.hashnode.dev/conditional-gradle-configuration-by-build-variant)
  * LinkedIn: [https://www.linkedin.com/pulse/conditional-gradle-configuration-build-variant-oliver-spryn](https://www.linkedin.com/pulse/conditional-gradle-configuration-build-variant-oliver-spryn)
  
* [**Adding Git Completion to Zsh**](./adding-git-completion-to-zsh/README.md) originally published on November 7, 2018
  * Blog: [https://oliverspryn.blog/adding-git-completion-to-zsh](https://oliverspryn.blog/adding-git-completion-to-zsh)
  * Medium: [https://oliverspryn.medium.com/adding-git-completion-to-zsh-60f3b0e7ffbc](https://oliverspryn.medium.com/adding-git-completion-to-zsh-60f3b0e7ffbc)
  * DEV Community: [https://dev.to/oliverspryn/adding-git-completion-to-zsh-26id](https://dev.to/oliverspryn/adding-git-completion-to-zsh-26id)
  * Hashnode: [https://oliverspryn.hashnode.dev/adding-git-completion-to-zsh](https://oliverspryn.hashnode.dev/adding-git-completion-to-zsh)
  * LinkedIn: [https://www.linkedin.com/pulse/adding-git-completion-zsh-oliver-spryn](https://www.linkedin.com/pulse/adding-git-completion-zsh-oliver-spryn)

* [**Add Chrome Custom Tabs to the Android Jetpack Navigation Component**](./android-navigation-component/README.md) originally published on June 20, 2019
  * Blog: [https://oliverspryn.blog/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component/](https://oliverspryn.blog/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component/)
  * Medium: [https://proandroiddev.com/add-chrome-custom-tabs-to-the-android-navigation-component-75092ce20c6a](https://proandroiddev.com/add-chrome-custom-tabs-to-the-android-navigation-component-75092ce20c6a)
  * DEV Community: [https://dev.to/oliverspryn/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component-484g](https://dev.to/oliverspryn/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component-484g)
  * Hashnode: [https://oliverspryn.hashnode.dev/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component](https://oliverspryn.hashnode.dev/add-chrome-custom-tabs-to-the-android-jetpack-navigation-component)
  * LinkedIn: [https://www.linkedin.com/pulse/add-chrome-custom-tabs-android-jetpack-navigation-component-spryn](https://www.linkedin.com/pulse/add-chrome-custom-tabs-android-jetpack-navigation-component-spryn)

* [**Writing a Fully Unit Testable Android App**](./android-unit-testing/README.md) originally published on December 19, 2020
  * Blog: [https://oliverspryn.blog/writing-a-fully-unit-testable-android-app](https://oliverspryn.blog/writing-a-fully-unit-testable-android-app)
  * Medium: [https://proandroiddev.com/writing-a-fully-unit-testable-android-app-90cd0310f18f](https://proandroiddev.com/writing-a-fully-unit-testable-android-app-90cd0310f18f)
  * DEV Community: [https://dev.to/oliverspryn/writing-a-fully-unit-testable-android-app-1eng](https://dev.to/oliverspryn/writing-a-fully-unit-testable-android-app-1eng)
  * Hashnode: [https://oliverspryn.hashnode.dev/writing-a-fully-unit-testable-android-app](https://oliverspryn.hashnode.dev/writing-a-fully-unit-testable-android-app)
  * LinkedIn: [https://www.linkedin.com/pulse/writing-fully-unit-testable-android-app-oliver-spryn](https://www.linkedin.com/pulse/writing-fully-unit-testable-android-app-oliver-spryn)

* [**The Handbook to GPG and Git**](./the-handbook-to-gpg-and-git/README.md) originally published on December 19, 2020
  * Blog: [https://oliverspryn.blog/the-handbook-to-gpg-and-git](https://oliverspryn.blog/the-handbook-to-gpg-and-git)
  * Medium: [https://medium.com/better-programming/a-handbook-to-gpg-and-git-5990f8db4361](https://medium.com/better-programming/a-handbook-to-gpg-and-git-5990f8db4361)
  * DEV Community: [https://dev.to/oliverspryn/the-handbook-to-gpg-and-git-2h65](https://dev.to/oliverspryn/the-handbook-to-gpg-and-git-2h65)
  * Hashnode: [https://oliverspryn.hashnode.dev/the-handbook-to-gpg-and-git](https://oliverspryn.hashnode.dev/the-handbook-to-gpg-and-git)
  * LinkedIn: [https://www.linkedin.com/pulse/handbook-gpg-git-oliver-spryn](https://www.linkedin.com/pulse/handbook-gpg-git-oliver-spryn)
