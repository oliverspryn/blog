# Metadata

- **Tags:** Android, Build Tool, Dynatrace, Gradle, Groovy
- **SEO Title:** Conditional Gradle Configuration by Build Variant
- **SEO Description:** Leverage the advantages of Groovy to conditionally include and exclude entire Gradle plugins based on your build type, and keep your build times low.
- **Kicker:** When Groovy becomes your Swiss army knife
- **Subtitle:** Optimize your build times and gain back precious minutes
