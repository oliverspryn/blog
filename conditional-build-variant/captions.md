# Captions

- By [Maximilian Weisbecker](https://unsplash.com/@maximilianweisbecker?utm_source=medium&utm_medium=referral) on [Unsplash](https://unsplash.com?utm_source=medium&utm_medium=referral)
- This snippet should make any Android engineer feel right at home
- We triplicated our build times with this additional configuration in our Gradle file
- By [Michael Mroczek](https://unsplash.com/@michaelmroczek?utm_source=medium&utm_medium=referral) on [Unsplash](https://unsplash.com?utm_source=medium&utm_medium=referral)
- This command intends to build an application in debug mode, and supposedly, without Dynatrace
- The build variant becomes part of a variant-specific task name, as seen in Android Studio
- By [NeONBRAND](https://unsplash.com/@neonbrand?utm_source=medium&utm_medium=referral) on [Unsplash](https://unsplash.com?utm_source=medium&utm_medium=referral)
- Dynatrace is properly excluded from the noDynatrace flavor
