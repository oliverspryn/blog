# Alternate Text

- Android Studio open on a Windows computer displaying Java code
- The back of a Google Pixel XL in the Very Silver color, laying on darkly stained wooden table
- The build variant window in Android Studio, highlighting the selected noDynatraceDebug variant
- A wooden hourglass with light brown sand sitting on a white tabletop surface against a white exposed brick wall
