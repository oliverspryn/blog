# Alternate Text

- New York City from the space
- A Google Pixel 2XL on a marble surface
- A developer workstation with two monitors, headphones, and an iPhone
- The home screen of a Samsung Galaxy phone
- A Samsung tablet sitting on a travel bag
- A working graph with fragments, an activity, and three CCT destinations
- The Chrome Custom Tabs component extension in action
