# Metadata

- **Tags:** Android, Navigation, Navigation Component, Chrome Custom Tabs, Android Jetpack
- **SEO Title:** Add Chrome Custom Tabs to the Android Jetpack Navigation Component
- **SEO Description:** Learn how to extend the Android Jetpack Navigation Component with a custom destination type by building a new Chrome Custom Tabs router.
- **Kicker:** Leveraging the open library to take it further
- **Subtitle:** Extending Jetpack's ability to navigate anywhere
