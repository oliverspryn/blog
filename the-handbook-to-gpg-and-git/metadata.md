# Metadata

- **Tags:** Cryptography, Git, GPG, Programming, Software Development
- **SEO Title:** The Handbook to GPG and Git
- **SEO Description:** Learn how to use GPG and Git for a more secure software development workflow without the hassle and overhead encryption normally adds to a project.
- **Kicker:** <None, per the request of Better Programming publications>
- **Subtitle:** A thorough guide to setting up a more secure development flow on your machine
