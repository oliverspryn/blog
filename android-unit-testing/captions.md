# Captions

- Photo by [Nguyen Dang Hoang Nhu](https://unsplash.com/@nguyendhn) on [Unsplash](https://unsplash.com)
- Photo by [Lance Anderson]((https://unsplash.com/@lanceanderson)) on [Unsplash](https://unsplash.com)
- My [sample project's](https://oliverspryn.link/android-unit-tests-repository) code coverage chart
- Grid charts showing coverage before I started writing tests and after I finished
- Photo by [Waldemar Brandt](https://unsplash.com/@waldemarbrandt67w) on [Unsplash](https://unsplash.com)
- The dependency injection architecture from inception to usage
- MainActivity injecting itself into the Dagger graph, then injecting its dependencies into itself
- The JaCoCo exclusion filter encompasses several broad categories of files and packages
- Photo by [Estée Janssens](https://unsplash.com/@esteejanssens) on [Unsplash](https://unsplash.com)
- The MainActivity is lean, simple, and in no need of unit test coverage
- The only instance of class instantiation outside of Dagger
- Photo by [Jesse Bowser](https://unsplash.com/@jessebowser) on [Unsplash](https://unsplash.com)
- A simplified DetailsController class in Kotlin
- A simplified DetailsController class in Java
- The coverage report for the DetailsController whenever null safety is not tested
