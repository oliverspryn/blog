# Alternate Text

- A standardized test answer sheet with a white pencil on top, post-it notepads, and a pencil sharpener nearby
- Detail on the Alcoa building in Pittsburgh, Pennsylvania
- A line chart showing how code coverage for my sample project increased over time
- Grid charts showing coverage before I started writing tests and after I finished
- A brightly-lit basement of a building showing the steel columns of the foundation holding up the higher floors
- A flowchart showing the Dagger architecture for this application
- A flowchart showing MainActivity injecting itself into the Dagger graph, then injecting its dependencies into itself
- A cup of coffee sitting on a daily planner notebook
- A winding road through a grassland in the countryside
- The coverage report for the DetailsController showing missing branch coverage whenever null safety is not tested
