# Metadata

- **Tags:** Android, Architecture, Kotlin, MVC, Unit Testing
- **SEO Title:** Writing a Fully Unit Testable Android App
- **SEO Description:** Learn the pillars of Android development that opens up the path for unit testing with a coverage rate of 95% or greater.
- **Kicker:** 95% and above coverage ratio is not a pipe dream
- **Subtitle:** The journey begins at the start of your project
